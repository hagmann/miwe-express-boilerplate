import express from "express";
import { getTodo, createTodo } from "../controllers/todoController";

const router = express.Router();

router.get("/todo", getTodo);
router.post("/todo", createTodo);

export default router;
